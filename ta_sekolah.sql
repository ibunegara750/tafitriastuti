-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2019 at 11:18 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta_sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(50) NOT NULL,
  `kategori_id` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `sampul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `user_id` int(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `kategori_id`, `judul`, `sampul`, `isi`, `user_id`, `created_at`, `updated_at`) VALUES
(4, '3', 'Siswa SMAN 1 Rasau Jaya Temukan Teknologi Listrik Alternatif', '1564586721_berita.png', '<p>Irvan dan Afifah merupakan anggota dari satu diantara tim yang ikut dalam perlombaan Parade Cinta Tanah Air kategori SMA yang di gelar Kementerian Pertahanan Provinsi Kalimantan Barat, Rabu (24/7) di Graha Teddy Kustari Lanud Supadio Kab Kubu Raya. Pada perlombaan itu keduanya yang merupakan siswa dari SMA Negeri 1 Rasau Jaya yang menampilkan proses terjadinya listrik alternatif yang dihasilkan menggunakan air gambut. Listrik alternatif ini diberi nama Nikipadang.Nikipadang adalah proyek yang bertujuan untuk memanfaatkan sumber daya alam, untuk dijadikan sebagai energi listrik alternatif.Nikipadang dalam hal ini menggunakan air gambut sebagai larutan elektrolit yang memiliki peran sebagai jembatan elektron antara elektroda-elektrodanya. Pemilihan gambut sebagai bahan utama dari penelitian ini adalah karena menurut hasil inventarisasi berbasis teknologi penginderaan jauh dan sistem informasi geografis tercatat seluas 1,729 juta ha atau 29,99% dari luas total wilayah dan air gambut memiliki kadar ph 4.0. Dengan perhitungan semakin rendah ph asam, maka sifat elektrolit pada asam akan semakin kuat. Sehingga air gambut merupakan bahan pemilihan yang tepat. Irvan menjelaskan perlu diketahui, air gambut tidak menghasilkan listrik. Tetapi air gambut digunakan sebagai larutan elektrolit yang berperan sama seperti asam sitrat pada jeruk lemon.</p>', 3, '2019-08-21 12:58:02', '2019-08-21 05:58:02'),
(5, '3', 'Peringatan Ulang Tahun Sekolah yang ke-24 SMAN 1 Rasau Jaya', '1564586875_berita1.png', '<p>Kemampuan SMAN 1 Rasau Jaya dalam menjaga kepercayaan pemerintah, masyarakat sebagai salah satu lembaga pendidikan yang senantiasa mencetak prestasi gemilang dan menyumbangkan alumni yang cerdas, tangguh, maupun di tingkat regional maupun nasional.<br />\r\n<br />\r\nGiat Peringatan HUT ke-24 ini merupakan langkah taktis yang ditunjukan kepada masyarakat luas untuk mengapresiasikan kinerja yang telah dilakukan SMAN 1 Rasau Jaya.<br />\r\n<br />\r\nDalam ulang tahun sekolah ini, SMAN 1 Rasau Jaya mengadakan kegiatan lomba antar sekolah dan siswa/i di lingkungan sekolah. Lomba yang diadakan, yaitu lomba non akademik, terdiri dari lomba antar sekolah cabang bola volley dan lomba antar sekolah cabang sepak bola serta penampilan pentas seni.<br />\r\n<br />\r\nPenampilan pentas seni terdiri dari lomba drama musikal, baca puisi, kriya, poster, hias kue tar, film pendek, monolog, kewirausahaan dan fotografi. Giat HUT ke-24 SMAN 1 Rasau Jaya diselenggarakan selama 5 hari, mulai dari tanggal 13-17 januari 2019.<br />\r\n<br />\r\nSekolah yang di undang untuk mengikuti kegiatan HUT SMAN 1 Rasau Jaya ini terdiri dari beberapa sekolah negeri dan swasta di daerah Kubu Raya, Pontianak dan sekitarnya. Sekolah tersebut antara lain SMKN 1 Rasau Jaya, SMAN 7 Pontianak, SMA Bayangkari, SMAN 2 Sungai Raya, SMAN 4 Pontianak, MA Raudatul Ulum, SMA Taman Mulia, SMKN 1 Sungai Raya, SMA Bina Mandiri, SMAN 1 Sungai Raya, SMAN 1 Sungai Ambawang dan SMAN 5 Pontianak.<br />\r\n<br />\r\nMenurut Ketua Penyelenggara sekaligus Pembina OSIS SMAN 1 Rasau Jaya, Paini,S.Pd menyatakan bahwa tujuan diadakannya HUT SMAN 1 Rasau Jaya ini agar dapat menjalin kerjasama antara SMAN 1 Rasau Jaya dengan insan pendidikan dan pemerhati pendidikan dalam penyelenggaraan pendidikan sebagai rintisan sekolah, Senin (14/1).<br />\r\n<br />\r\n&quot;Selain itu, tujuan diadakan HUT SMAN 1 Rasau Jaya ini untuk memupuk jiwa semangat yang senantiasa menunjang tingginya pemahaman akan pentingnya pendidikan sebagai modal utama dalam merintis kejayaan dan keberhasilan, serta menjalin silaturahim dan mempererat rasa persaudaraan antara SMAN 1 Rasau Jaya dengan sekolah-sekolah di wilayah kabupaten Kubu Raya khusunya,&quot; tukasnya.</p>', 3, '2019-08-22 05:59:59', '2019-08-21 22:59:59'),
(6, '3', 'Ekstrakurikuler Pramuka pada SMAN 1 Rasau Jaya', '1565219405_67151571_436026653912940_2235206349271072768_n.jpg', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>\r\n\r\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.</p>\r\n\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.</p>\r\n\r\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.</p>\r\n\r\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.</p>\r\n\r\n<p>&nbsp;</p>', 3, '2019-08-22 06:02:00', '2019-08-21 23:02:00'),
(7, '3', 'Mbah Nardi Seorang Kapiten', '1566749149_ied mubarak.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>\r\n\r\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.</p>\r\n\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.</p>\r\n\r\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.</p>\r\n\r\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.</p>', 3, '2019-08-25 09:05:53', '2019-08-25 09:05:53');

-- --------------------------------------------------------

--
-- Table structure for table `beritafile`
--

CREATE TABLE `beritafile` (
  `id` int(50) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beritafile`
--

INSERT INTO `beritafile` (`id`, `judul`, `isi`, `created_at`, `updated_at`) VALUES
(1, 'Pengumuman Penerimaan Siswa Baru 2019', 'https://docs.google.com/spreadsheets/d/1B0x19Y9YEy6N-_OKvp5gym-bL8dSn4sAX87LP4qLNew/edit#gid=0', '2019-08-21 22:25:42', '2019-08-21 22:25:42');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(50) NOT NULL,
  `siswa` varchar(50) NOT NULL,
  `guru` varchar(50) NOT NULL,
  `akan_lulus` varchar(50) NOT NULL,
  `alumni` varchar(50) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `siswa`, `guru`, `akan_lulus`, `alumni`, `isi`, `foto`, `created_at`, `updated_at`) VALUES
(1, '600', '44', '178', '2500', 'Sekolah Menengah Atas Negeri 1 Rasau Jaya yang meningkatkan prestasi dibidang akademik, seni dan olahraga dengan ikut serta secara aktif dalam event-event lomba yang diadakan. Melaksanakan pelatihan keterampilan dalam penguasaan teknologi informasi dan komunikasi sebagai bekal untuk mengikuti pendidikan lebih lanjut.', '1566926003_smansa1.jpg', '2019-08-27 17:13:24', '2019-08-27 10:13:24');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(15) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(3, 'Tentang Sekolah', '2019-07-31 18:09:02', '2019-07-31 11:09:02'),
(5, 'Pengumuman Penting', '2019-07-25 03:37:18', '2019-07-25 03:37:18'),
(6, 'Jadwal Ulangan', '2019-08-07 16:02:43', '2019-08-07 16:02:43'),
(7, 'Format Excel', '2019-08-21 20:48:00', '2019-08-21 20:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id` int(15) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `kelas` varchar(191) NOT NULL,
  `semester` varchar(10) NOT NULL,
  `lampiran` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id`, `nama`, `kelas`, `semester`, `lampiran`, `created_at`, `updated_at`) VALUES
(2, 'Geografi', 'XI IPS I', 'Semester 2', 'PROPOSAL.pdf', '2019-08-03 15:55:48', '2019-08-03 08:55:48'),
(3, 'Sosiologi', 'XI IPA III', 'Semester 1', 'ANGKET PENILAIAN.pdf', '2019-08-05 03:12:13', '2019-08-05 03:12:13'),
(4, 'Kimia', 'XI IPA 1', 'Semester 1', 'Form Wawancara Alumni Siswa SMA Negeri 1 Rasau Jaya.pdf', '2019-08-07 16:04:54', '2019-08-07 16:04:54');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(11) NOT NULL,
  `no_surat` varchar(191) NOT NULL,
  `jns_kegiatan` varchar(191) NOT NULL,
  `pelaksana` varchar(191) NOT NULL,
  `wkt_pelaksanaan` date NOT NULL,
  `pj` varchar(191) NOT NULL,
  `hasil` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id`, `no_surat`, `jns_kegiatan`, `pelaksana`, `wkt_pelaksanaan`, `pj`, `hasil`, `created_at`, `updated_at`) VALUES
(3, 'A01', 'OSN', 'Panglima Pro', '2019-10-11', 'Ibu Irma', 'Juara 1', '2019-07-31 07:08:33', '2019-07-31 07:08:33'),
(4, 'A02', 'Lomba Fisika', 'PanglimaPro', '2019-10-12', 'Ibu Irma', 'Juara 3', '2019-07-31 07:43:22', '2019-07-31 07:43:22'),
(5, 'A03', 'Lomba Puisi Tingkat Kabupaten', 'PanglimaPro', '2019-10-13', 'Ibu Suryani', 'Juara 2', '2019-07-31 08:14:02', '2019-07-31 08:14:02'),
(6, 'A04', 'Lomba Kimia', 'PanglimaPro', '2019-10-14', 'Ibu Suryani', 'Juara 2', '2019-07-31 08:15:24', '2019-07-31 08:15:24'),
(7, 'A05', 'ASN', 'PanglimaPro', '2019-10-15', 'Ibu Suryani', 'Juara 1', '2019-07-31 08:16:09', '2019-07-31 08:16:09'),
(8, 'A06', 'Lomba', 'Pro Panglima', '2019-10-12', 'Ibu Suryani', 'Juara 5', '2019-08-05 03:16:58', '2019-08-05 03:16:58');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(50) NOT NULL,
  `selayang` text NOT NULL,
  `visi` varchar(300) NOT NULL,
  `misi` text NOT NULL,
  `foto_struktur` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `selayang`, `visi`, `misi`, `foto_struktur`, `created_at`, `updated_at`) VALUES
(3, 'SMA Negeri 1 Rasau Jaya merupakan satu-satunya SMA Negeri di kecamatan Rasau Jaya kabupaten Kubu Raya yang memiliki akreditas sekolah  A. SMA Negeri 1 Rasau Jaya terletak di Jalan Pendidikan No.6 Rasau Jaya 1 dan memiliki  52 staff pengajar PNS/non PNS serta lebih dari 600 orang siswa sekolah diperkirakan didirikan sejak 1995. Dua puluh empat tahun SMAN 1 Rasau Jaya telah mengabdikan diri di bidang pendidikan merupakan usia yang panjang dan cukup matang bagi sebuah lembaga pendidikan untuk mencapai predikat sekolah favorit di Rasau Jaya. Kemampuan SMAN 1 Rasau Jaya dalam menjaga kepercayaan pemerintah, masyarakat sebagai salah satu lembaga pendidikan yang senantiasa mencetak prestasi gemilang dan menyumbangkan alumni yang cerdas, tangguh, maupun di tingkat regional maupun nasional', 'Beriman, Bertaqwa, Cerdas, Terampil, Mandiri', 'Melaksanakan pembelajaran dan bimbingan secara efektif, efisien sehingga setiap siswa berkembang sesuai dengan potensi yang dimilikinya.\r\nMewujudkan penghayatan terhadap ajaran Agama yang dianut untuk meningkatkan iman dan takwa serta kepribadian yang mulia sebagai dasar berpikir, berkata dan berbuat yang sesuai dalam kehidupan sehari-hari.\r\nMelaksanakan pembinaan dibidang seni sesuai dengan bakat dan potensi yang dimiliki setiap siswa, sehingga mampu tampil di masyarakat.\r\nMelaksanakan pembinaan dan pelatihan olahraga sesuai dengan bakat dan potensi yang dimiliki siswa, sehingga memiliki daya fisik sehat, tangguh dan tumbuh rasa sportifitas dalam dirinya.\r\nMelaksanakan pembinaan dan pelatihan Pramuka agar terwujud siswa yang berkarakter.\r\nMelaksanakan pelatihan keterampilan dalam penguasaan teknologi informasi dan komunikasi sebagai bekal untuk mengikuti pendidikan lebih lanjut.\r\nMenetapkan manajemen partisipatif dengan melibatkan seluruh warga sekolah dan masyarakat, khususnya orang tua siswa.', '1566389679_Struktur Organisasi SMANSA RAJA.png', '2019-08-21 12:39:12', '2019-08-21 05:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'high level user', NULL, NULL, NULL),
(2, 'anggota', 'general user', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(50) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `judul`, `isi`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Selamat Datang di Website SMA Negeri 1 Rasau Jaya', 'Melaksanakan proses belajar mengajar secara efektif, efisien dan bermutu sesuai dengan perkembangan zaman', '1566443588_1.jpg', '2019-08-22 03:13:11', '2019-08-21 20:13:11'),
(2, 'Selamat Datang di Website SMA Negeri 1 Rasau Jaya', 'Beriman, Bertaqwa, Cerdas, Terampil, Mandiri', '1566411806_67151571_436026653912940_2235206349271072768_n.jpg', '2019-08-21 11:23:26', '2019-08-21 11:23:26'),
(3, 'Selamat Datang di Website SMA Negeri 1 Rasau Jaya', 'Mewujudkan peningkatan kualitas lulusan yang memiliki sikap, pengetahuan, dan keterampilan yang seimbang, serta meningkatkan jumlah lulusan yang melanjutkan ke perguruan tinggi', '1566443670_7.jpg', '2019-08-21 20:14:33', '2019-08-21 20:14:33');

-- --------------------------------------------------------

--
-- Table structure for table `tenaga_pendidik`
--

CREATE TABLE `tenaga_pendidik` (
  `id` int(11) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `jabatan` varchar(191) NOT NULL,
  `mapel` varchar(191) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tenaga_pendidik`
--

INSERT INTO `tenaga_pendidik` (`id`, `nama`, `jabatan`, `mapel`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Sukarni', 'Kepala Sekolah', '-', '1564805750_Active-Herbal.png', '2019-07-21 17:42:28', '2019-08-02 21:15:52'),
(2, 'Sukamto', 'Komite Sekolah', '-', '1564805826_Active-Herbal.png', '2019-07-31 05:05:18', '2019-08-02 21:17:07'),
(3, 'Sri Nugroho', 'Operator', 'Olahraga', '1564805911_Active-Herbal.png', '2019-07-31 05:27:45', '2019-08-02 21:18:32'),
(4, 'H. Supardi', 'Tata Usaha', '-', '1564806023_Active-Herbal.png', '2019-07-31 05:28:57', '2019-08-02 21:20:23'),
(5, 'Sholihan', 'Waka UR Kurikulum', 'Agama Islam', '1564806081_paksolihan.jpg', '2019-07-31 05:30:40', '2019-08-02 21:21:22'),
(6, 'Suas Maji', 'Waka UR Prasarana', 'Ekonomi', '1564806284_pak suas.jpg', '2019-08-02 21:24:44', '2019-08-02 21:24:44'),
(7, 'Alif Syaifudin', 'Waka UR Kesiswaan', 'Geografi', '1564806417_pakalif.jpg', '2019-08-02 21:26:57', '2019-08-02 21:26:57'),
(8, 'Dwi Budiningtyas', 'Waka UR Humas', 'Pkn', '1564806536_829453_user_512x512.png', '2019-08-02 21:28:57', '2019-08-02 21:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_akses` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_akses`, `name`, `email`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Admin', 'Fitri Astuti', 'fitriastuti750@gmail.com', '$2y$10$Eo8POaq43LLh9pKBf7G3VOmLo9T.vA27whyR6qam4HljcCF2JMO9u', NULL, 'BSogT9EOvpvk10khS75dm61tUvcIbvs5Ke2lhymNq4iUVUBfsq41PLIqJjV7', '2019-07-24 03:54:45', '2019-07-24 03:54:45'),
(5, 'Guru', 'ibunegara', 'ibunegara@gmail.com', '$2y$10$VaiVgPJInQmCKVnmND1oYOLHYfka4JUu/u5xOJDuxUtCRz25kYlK.', NULL, 'YKLDowLXWdJAXK6BVWiHKI0zxdb2ZKBmnjGqwBNMOI8lmFpqW4NMW3uZ7qT3', '2019-08-21 07:09:32', '2019-08-21 07:09:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `beritafile`
--
ALTER TABLE `beritafile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `roles_name_unique` (`name`) USING BTREE;

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenaga_pendidik`
--
ALTER TABLE `tenaga_pendidik`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `beritafile`
--
ALTER TABLE `beritafile`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tenaga_pendidik`
--
ALTER TABLE `tenaga_pendidik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
